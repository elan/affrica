# Projet Africa : des transcriptions textes à l'apparat en XML-TEI
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Felan%2Faffrica.git/HEAD)

## Données de départ : instructions
Les transcriptions initiales des manuscrits doivent être présentées dans le dossier `data/` avec un fichier txt par transcription. Ces fichiers doivent être encodées en UTF-8.

_/* à compléter pour voir si un logiciel peut être plus adapté qu'un autre... */_

Les fichiers doivent suivent le plan de nommage suivant : `Africa_XX_YYY`, avec XX le numéro du livre sur 2 caractères (01, 02... 10) et YYY le code du manuscrit. Par exemple, pour la transcription du livre 1 du manuscrit A, le fichier aura pour nom : `Africa_01_A.txt`.

Les transcriptions doivent se présenter comme sur l'extrait suivant :
```txt
FOLIO [1r]
eT mihi conspicuum meritis belloque tremendum
Musa uirum referes. italis cui fracta sub armis
Nobilis eternum prius attulit africa nomen.
Hunc precor exhausto liceat mihi surgere fontem
Ex helicone sacro dulcis mea cura sorores
Si uobis miranda cano. Iam ruris amici
```

* Une ligne correspond à un vers
* Les folios peuvent être indiqués sous la forme suivante :
    * `FOLIO XXX YYY` où XXX est optionnel et correspond au numéro/nom du folio sans aucun espace (par exemple, `[1r]`ou `f.1.1`  et YYY est aussi optionnel et correspond à une URL menant vers le facsimilé.
    * Cette ligne est ensuite utilisée pour extraire 1, 2 ou 3 informations et utilisant l'espace comme séparateur. XXX et YYY ne doivent donc pas contenir d'espace
    * Indiquer les folios est optionnel mais conseillé
    * Les folios sont indiqués sur leur propre ligne.
    * Numéro et URL sont optionnels mais il n'est pas possible d'indiquer une URL si on n'indique pas de nom.
    * Exemples :
      * `FOLIO` : indique simplement qu'ici on a le début d'un folio
      * `FOLIO [1r]` : indique un FOLIO et son nom
      * `FOLIO f._1r https://gallica.bnf.fr/ark:/12148/btv1b10721066q/f6.item` : indique un FOLIO, son nom et son URL
* Ne pas intégrer de saut de ligne (pas de ligne vide).

## Etape 1 : Sélection du corpus et collation automatique : `Africa_pre-coll-tei.ipynb`
Ouvrir le fichier `Africa_pre-coll-tei.ipynb`. Il permet d'opérer un ensemble de pré-traitement, traitements et post-traitements sur les données, mais aussi de configurer les entrées et sorties (sections «&nbsp;Définition du corpus&nbsp;» et «&nbsp;Définition des sorties à produire&nbsp;»).

Pour ce faire, lancer les étapes du programme en se positionnant sur la première case et en faisant Shift+Entrée jusqu'à atteindre la fin des traitements. Au fur-et-à-mesure, observer les résultats intermédiaires et modifier les valeurs selon :
* la collation voulue dans la section «&nbsp;Définition du corpus&nbsp;»
* les sorties voulues dans la section «&nbsp;Définition des sorties à produire&nbsp;». Actuellement, 2 sorties sont possibles :
    * une sortie tabulaire sous la forme d'un fichier HTML et pour laquelle deux versions sont possibles. Une version dite "uniq" qui met en valeur les variantes uniques et une version "color" qui offre une coloration par groupe de variantes (pour un maximum de 5 groupes ; si plus de 5 groupes sont présents, les groupes excédants apparaitront sur fond blanc)
    * une sortie TEI. Le fichier est bien formé et conforme à la TEI mais présente un teiHeader très minimal et doit être enrichi dans sa totalité

## Etape 2 : Observation des résultats
Dans la colonne de droite, ouvrir dans le dossier `output/` les fichiers générés :
* les fichiers HTML sont visibles directement dans un navigateur web
* les fichiers XML peuvent être modifiés/complétés sur un éditeur XML (comme oXygenXML Editor). Il peuvent aussi être utilisés sur l'outil TEI Critical Apparatus Toolbox pour visualiser les versions parallèles
  * voir (le site de TEI CAT)[http://teicat.huma-num.fr/witnesses.php]
 
Les fichiers peuvent être sauvegardés sur son propre ordinateur par un clic-droit puis «&nbsp;Download&nbsp;»

## Etape 3 : Complétion des fichiers TEI
AnneGF : À vous de décider de votre protocole et répartition du travail, je peux simplement conseiller de ne s'y mettre qu'à partir du moment où vous avez des données initiales satisfaisantes : l'ensemble de vos témoins et une déjà assez bonne relecture des transcriptions. Bien sûr, des corrections sont toujours possibles mais un encodage TEI des variantes constitue un fichier lourd au sein du quel le texte est parfois assez peu lisible. Le cas qui pourra poser plus que question que des corrections ponctuelles est celle de l'ajout d'un témoin. En effet, si vous souhaitez en ajouter un à postériori, il faudra soit recommencer les traitements et donc "perdre" les modifications faites sur les fichiers TEI, soit travailler directement dans le fichier TEI.

La documentation de la TEI est tout à fait bien faite, de nombreux supports de formation sont disponibles et des formations sont régulièrement organisées.

_/* ajouter ici des liens ?!? */_

Pour des questions plus ponctuelles, la liste de diffusion est très utiles, tant pour les grands débutants que pour les personnes expertes.

Il vous faudra aussi vous mettre d'accord sur le travail à effectuer : typage des variantes, ajout des gloses, etc.

Pensez aussi à indiquer votre rôle dans le teiHeader (`<respStmt/>`, `<revisionDesc/>`, `<change/>`...).

## Autres exploitations des données
## Création d'un stemma automatique
Les travaux de Jean-Baptise Camps permettent en utilisant le logiciel R de tester la création d'un stemma.

Voir : https://github.com/Jean-Baptiste-Camps/stemmatology


## Création d'une édition numérique
Plusieurs logiciels existent dont par exemple EVT et teiPublisher.

Voir https://wiki.tei-c.org/index.php?title=Category:Publishing_and_delivery_tools

## Etc, etc.
De nombreux autres possibilités existent. Le wiki du consortium TEI propose un ensemble très vaste d'outils.

Voir https://wiki.tei-c.org/index.php/Category%3ATools