import os
import re
import json
from operator import countOf
import xml.dom.minidom

def create_customized_files(text_files, init_line, final_line):
    partial_dir="partial_data2/"
    os.makedirs(os.path.dirname(partial_dir), exist_ok=True)
    inputfiles=[]
    for f in text_files:
        num_line = 0
        outfilename = partial_dir+"l"+str(init_line)+"-"+str(final_line)+"_"+os.path.basename(f)
        out = open(outfilename, "w")
        with open(r''+f, 'r') as fp:
            for i, line in enumerate(fp):
                if not line.startswith("FOLIO"):
                    num_line+=1
                if num_line == init_line:
                        print("\t"+outfilename+"\tcorrespond à\t"+f+"\tde la ligne\t"+str(i), end='')
                if num_line >= init_line and num_line <= final_line:
                    #print(num_line)
                    out.write(line.strip()+'\n')
                elif num_line > final_line:
                    print("\tà la ligne\t"+str(i))
                    break
        out.close()
        inputfiles.append(outfilename)
    return inputfiles


def collatex_text_to_json(text_files, folio_is_token=False, token_linebreak=False):
    # this function is adapted from a first version by Elisa Nury
    # witnesses list
    listwit = []
    
    # for each file in textfiles
    for path in text_files:        
        # witness dict
        wit = {}
        
        # get filename
        filename = os.path.basename(path)
        
        # witness identifier (siglum) from filename
        wit['id'] = os.path.splitext(filename)[0]
        
        # list of witness tokens
        wit['tokens'] = []
        
        # open file
        with open(path, 'r', encoding='utf-8') as file:
            
            # folio value
            folio_no = ''
            folio_url = ''
            newfolio = False
            
            # read line by line
            for line in file:
                
                # if folio line
                if line.startswith('FOLIO'):
                    
                    # update folio number and url
                    elem = line.split(' ')
                    FOLIO = elem[0]
                    folio_no = ""
                    folio_url = ""
                    if len(elem) > 1:
                        folio_no = elem[1]
                        
                    if len(elem) > 2:
                        folio_url = elem[2]
                        
                    newfolio = True
                    
                    # if the folio should be a token
                    if folio_is_token:
                        
                        # create token
                        t = {'t': FOLIO, 'n': FOLIO, 'folio_no':folio_no, 'folio_url':folio_url, 'newfolio': newfolio}
                        
                        # append token to the list
                        wit['tokens'].append(t)
                
                else:
                    
                    #enury: separate words at whitespace
                    #listwords = line.split(' ') #annegf commented
                    #annegf tests
                    #listwords = re.split('\W', line)
                    #listwords = filter(lambda x: x.strip(), listwords)
                    listwords = re.findall(r"[\w']+|[.,;:!?]", line)
                    #end annegf tests
                    # for each word
                    for word in listwords:
                         
                        # remove endline
                        token = word.split('\n')
                        
                        # normalisation. Here lowercase. (also possible to add rules for punctuation marks)
                        #n = token[0].lower() #annegf removed
                        n = token[0].lower().replace('u', 'v')
                        #print(token, n)

                        # create a token
                        t = {'t': token[0], 'n': n, 'folio_no':folio_no, 'folio_url':folio_url, 'endline':False if len(token) == 1 else True, 'newfolio': newfolio}
                        
                        # append token to the list
                        wit['tokens'].append(t)
                        
                        # we are not anymore at the start of a new folio
                        newfolio = False
                        
                    # if endline is a token
                    if token_linebreak:
                        t = {'t': 'LINE', 'n': '\n', 'folio_no':folio_no, 'folio_url':folio_url, 'endline':True, 'newfolio': newfolio}
                        wit['tokens'].append(t)
        
    
    
        # add to witnesses list
        listwit.append(wit)
    
    # return output as json
    json_witnesses = {'witnesses': listwit}
    return json_witnesses


def collatex_result_to_html(result, style="color"):
    #HTML supposing bootstrap is used
    html_table = '<html><head><meta charset="utf-8"/><meta name="viewport" content="width=device-width, initial-scale=1"/>\n<!-- Bootstrap CSS -->\n<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"/></head><body><div class="col-10 offset-1">'
    if style == "color":
        html_table += '<style>.group1 { background-color: darkseagreen !important; } .group2 { background-color: cornflowerblue !important  } .group3 { background-color: sandybrown !important  } .group4 { background-color: lightgreen !important; } .group5 { background-color: pink !important; } .header { position: sticky; top: 0; }</style>'
    elif style == "uniq":
        html_table += '<style>.diff { background-color: pink !important;} .thediff { background-color: tomato !important;} .header { position: sticky; top: 0; }</style>'

    html_table += '<table class="table"><thead style="position: sticky;top: 0" class="bg-dark text-light"><tr>'
    
    witnesses = json.loads(result)['witnesses']
    witrange = range(len(witnesses))
    
    for wit in witnesses:
        html_table += '<th class="header" scope="col">'+wit+'</th>'
    html_table += '</tr></thead><tbody>'
    
    for row in list(zip(*json.loads(result)['table'])):
        
        html_table += '<tr>'
        n_all = list(set(row[i][0]['n'] for i in witrange if row[i] is not None))
        
        # folio lines
        # it checks only the first token in the list 
        # (but we assume here only one token per cell because of the segmentation)
        if any(row[i][0]['t'] == 'FOLIO' for i in witrange if row[i] is not None):
            for cell in row:
                if cell is None:
                    string = ''
                else:
                    folio = "FOLIO" if cell[0]['folio_no'] == '' else cell[0]['folio_no']
                    string = folio if cell[0]['folio_url'] == '' else '<a href="'+cell[0]['folio_url']+'">f.'+cell[0]['folio_no']+'</a>'
                
                html_table += '<td style="">'+string+'</td>' if cell is None else '<td style="border-top:5px double;">'+string+'</td>'
    
        # endlines
        elif any(row[i][0]['t'] == 'LINE' for i in witrange if row[i] is not None):
            for cell in row:
                html_table += '<td style="background-color:gold;"></td>'
    
        # others
        else:
            # set background color according to variation
            # if there is an empty cell, there is a variation
            if any(row[i] == None for i in witrange):
                bgcolor = '#FF7F7'
                htmlclass = "diff"
            # otherwise
            else:
                # list of token t values
                t = [row[i][0]['n'] for i in witrange] #annegf changed 't' to 'n'
                # if the list has more than one value, there is a variation
                if len(set(t)) > 1:
                    bgcolor = '#FF7F7'
                    htmlclass = "diff"
                else:
                    bgcolor = ''
                    htmlclass = ""
            #annegf
            normalized = []
            for cell in row:
                n = "-" if cell is None else cell[0]['n']
                normalized.append(n)
            #end annegf
            # create html cells
            for cell in row:
                string = '-' if cell is None else cell[0]['t']
                
                #annegf
                n = '-' if cell is None else cell[0]['n']
                
                thisclass = ' group'+str(n_all.index(n)+1) if cell is not None and len(n_all) > 1 else ''
                
                if countOf(normalized, n) == 1 and style == "uniq":
                    string='<b>'+string+'</b>'
                    html_table += '<td class="thediff">'+string+'</td>'
                else:
                    html_table += '<td class="'+htmlclass+thisclass+'">'+string+'</td>'
                
                #html_table += '<td bgcolor="'+bgcolor+'">'+string+'</td>'#annegf commented
            html_table += '</tr>'

    html_table += '</tbody></table>'
    html_table += '</div></body></html>'
    return html_table


def get_tei_conform_xml(result_tei, collation_name, witness_xmlid_prefix, witnesses):
    # Some cleaning
    clean_tei = re.sub('[ \t]+', ' ', result_tei.replace('\n', ' ')).replace('LINE', '</l><l>').replace('xmlns="http://www.tei-c.org/ns/1.0">','xmlns="http://www.tei-c.org/ns/1.0"><l>')
    clean_tei = clean_tei.replace('<l> </cx:apparatus>', '</cx:apparatus>').replace('<app>','<app type="todo">')
    #clean_tei = re.sub('(<app> *<rdg wit="[^>]+">)</l><l>(</rdg> *</app>)', '\g<1><lb/>\g<2>', clean_tei)
    clean_tei = re.sub('(<rdg wit="[^>]+">)</l><l>(</rdg>)', '\g<1><lb/>\g<2>', clean_tei)
    xml_p = xml.dom.minidom.parseString(clean_tei)

    # Create a TEI conformant file
    root = xml_p.createElementNS("http://www.tei-c.org/ns/1.0","TEI")
    xmlns = xml_p.createAttribute("xmlns")
    xmlns.value = "http://www.tei-c.org/ns/1.0"
    root.setAttributeNode(xmlns)
    teiHeader_node = xml_p.createElement("teiHeader")
    fileDesc_node = xml_p.createElement("fileDesc")
    titleStmt_node = xml_p.createElement("titleStmt")
    title_node = xml_p.createElement("title")
    title_str = xml_p.createTextNode(collation_name)
    publicationStmt_node = xml_p.createElement("publicationStmt")
    publication_p_node = xml_p.createElement("p")
    publication_p_str = xml_p.createTextNode("Publication Information")
    sourceDesc_node = xml_p.createElement("sourceDesc")
    listWit_node = xml_p.createElement("listWit")
    for wit in witnesses:
        witness_node = xml_p.createElement("witness")
        xmlid = xml_p.createAttribute("xml:id")
        xmlid.value = witness_xmlid_prefix+wit
        witness_node.setAttributeNode(xmlid)
        listWit_node.appendChild(witness_node)
    
    sourceDesc_node.appendChild(listWit_node)
    publication_p_node.appendChild(publication_p_str)
    publicationStmt_node.appendChild(publication_p_node)
    title_node.appendChild(title_str)
    titleStmt_node.appendChild(title_node)
    fileDesc_node.appendChild(titleStmt_node)
    fileDesc_node.appendChild(publicationStmt_node)
    fileDesc_node.appendChild(sourceDesc_node)
    teiHeader_node.appendChild(fileDesc_node)
    root.appendChild(teiHeader_node)
    
    text_node = xml_p.createElement("text")
    #body_node = xml_p.getElementsByTagName("body")
    body_node = xml_p.createElement("body")
    
    # Replace <cx:apparatus/> by <text/> node
    #for n in xml_p.getElementsByTagName("cx:apparatus"):
    #    body_node.appendChild(n)
    text_node.appendChild(xml_p.getElementsByTagName("cx:apparatus")[0])
    root.appendChild(text_node)
    #xml_p.replaceChild(root,xml_p.getElementsByTagName("body")[0]); 
    
    
    # To get a nice output
    pretty_xml = root.toprettyxml()
    pretty_xml = os.linesep.join([s for s in pretty_xml.splitlines() if s.strip()]) #remove blank lines
    
    return pretty_xml


