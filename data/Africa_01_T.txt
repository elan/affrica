FOLIO [1r]
Et michi conspicuum meritis belloque tremendum
Musa uirum referes ! ytalis cui fracta sub armis
Nobilis eternum prius attulit africa nomen 
Hunc precor exhausto liceat michi sugere fontem
Ex elicone sacrum dulcis mea cura sorores
Si uobis miranda cano ! Iam ruris amici
Prata quidem et fontes uacuisque silentia campis
Fluminaque et colles et apricis ocia siluis
Restituit fortuna michi uos carmina uati
Reddite uos animos ¶ tuque o certissima mundi
Spes superumque decus quem secula nostra deorum
Victorem atque herebi memorant quem quina uidemus
Larga per innocuum retegentem uulnera corpus
Auxilium fer summe parens tibi multa reuertens
Vertice parnasi referam pia carmina si te
Carmina delectant . uel si minus illa placebunt
Forte etiam lacrimas . quas sic mens fallitur olim
Fundendas longo demens tibi tempore seruo 
Te quoque trinacrii moderator maxime regni
Hesperieque decus atque eui gloria nostri
Iudice quo merui uatumque in sede sedere
Optatasque diu lauros titulumque poete
Te precor oblatum tranquillo pectore munus
Hospicio dignare tuo nam cuncta legenti
Forsitan occurret uacuas quod mulceat aures
Peniteatque minus suscepti in fine laboris 
Preterea in cunctos pronum sibi feceris annos
Posteritatis iter quis enim damnare sit ausus
Quod uideat placuisse tibi : fidentius ista
Arguit expertus nutu quem simplice dignum
Effecisse potes quod non erat : aspice templis
Dona sacris affixa parens ut uulgus adoret
Exime : despiciet quantum tua clara fauori
Fama meo conferre potest : modo mitis in umbra
Nominis ista tui dirum spretura uenenum
Inuidie latuisse uelis ubi nulla uetustas
FOLIO [1v]
Interea et nulli rodent mea numina uermes
Suscipe Iamque precor regum inclite suscipe tandem
Atque pias extende manus et lumina flecte.
Ipse tuos actus meritis ad sidera tollam
Laudibus . atque alio fortasis carmine condam
Mors modo me paulum expectet non longa petuntur
Nomen et alta canam siculi miracula regis
Non audita procul sed que modo uidimus omnes
Omnia namque solent similis quos cura fatigat
Longius isse retro . tenet hos millesimus annus
Solicitos . pudet hac alios consistere metha .
Nullus ad etatem propriam respexit ut erret
Musa parum notos nullo prohibente per annos
Liberior ! troiamque ideo canit ille ruentem
Ille refert thebas iuuenemque occultat achillem
Ille autem emathiam romanis ossibus implet
Ipse ego non nostri referam modo temporis acta
Marte sed ausonio sceleratos funditus afros
Eruere est animus ! nimiasque retundere uires
At semper te corde gerens properansque reuerti
Rex iter hoc ingressus agam . tua maxima facta
Non ausus tetigisse prius : Magis illa trahebant
Sed tremui me teque uidens . atque omnia librans
Ingenium tentare libet . si forte secundis
Cesserit auspiciis solidis tunc uiribus alta
Aggrediar namque ipse aderis ! meque ampla uidebit
Inclita parthenope redeuntem ad menia rursus
Et romana iterum referentem serta poetam
Nunc teneras frondes humili de stipite uulsi
Scipiade egregios primos comitante paratus
Tunc ualidos carpam ramos tu nempe iuuabis
Materia generose tua . calamumque labantem
Firmabis : meritumque decus continget amanti
Altera temporibus pulcherrima laurea nostris
Que tantis sit causa malis que cladis origo
Queritur . unde animi quis tot tolerare coegit
FOLIO [2r]
Dura pererrato ualidas furor equore gentes
Europamque dedit libie . libiamque rebellem
Europe alterno uastandas turbine terras
Ac michi causa quidem studii non indiga longi
Occurrit radix cunctorum infecta malorum
Inuidia unde oriens extrema ab origine mors est
Atque aliena uidens tristi dolor omnia uultu
Prospera non potuit florentem cernere romam
Emula carthago surgenti inuiderat urbi
Sed grauius tulit inde parem : mox uiribus auctam
Vidit et imperio domine parere potentis
Ac leges audire nouas et ferre tributum
Edidicit : tacitis intus sed plena querelis
Plena minis frenum funesta superbia tandem
Compulit excutere . et clades geminare receptas
Angebant dolor atque pudor seruilia passos
Multa uiros ! animisque incesserat addita duris
Tristis auaritia . et nunquam satiabile uotum
Premixte spes amborum optatumque duobus
Imperium populis dignus sibi quisque uideri
Omnia cui subsint totus cui pareat orbis
Pretera damnumque recens iniuriaque atrox
Insula sardinie amissa et trinacria rapta
Atque yspana nimis populo confinis utrique
Omnibus exposita insidiis aptissima prede
Terra tot infandos longum passura labores
Haud aliter quam cum medio deprhensa luporum
Pinguis ouis . nunc huc rapidis nunc dentibus illuc
Voluitur Inque tremens pates descerpitur omnis
Bellantum proprioque madens resupina cruore
Accessit situs ipse loci natura locauit
Se procul aduerso spectantes littore gentes
Aduersosque animos aduersas moribus urbes
Aduersosque deos . odiosaque nomina utrinque
Pacatique nichil uentos elementaque prorsus
Obuia et infestos luctantes equore fluctus
FOLIO [2v]
Ter grauibus certatum odiis et sanguine multo
At ceptum primo profligatumque secundo
Est bellum si uera notes nam tertia nudus
Prelia finis habet modico confecta labore
Maxima nos rerum hic sequimur mediosque tumultus
Eximiosque duces et inenarrabile bellum.
Vltima sidereum iuuenem lassata procellis
Hesperia . excusamque graui ceruice cathenam
Ausoniumque iugum romanaque senserat arma
Iam fuga precipites longe trans equora penos
Egerat ! horruerant animos dextramque tonantis
Fulmineam moresque ducis famamque genusque
Armorumque nouas artes : atque orsa cruentis
Nobilitata malis . Vix tandem littore mauro
Perfidus urgentem respectans hasdrubal hostem
Tutus erat sic uenantum perterritus acrem
Respicit atque canum ceruus post terga tumultum
Montis anhela procul de uertice colla reflectens
Constitit occeano domitor telluris hibere
Qua labor ambiguus uatum pelagique columnas
Verberat herculeas . ubi fessus mergitur alto
Phebus et estiuo detergit puluere currum
Hic ubi non uis ulla manu mortalis at ipsa
Omnipotens aduersa aditum natura negabat
Constitit atque auidis prereptum facibus hostem
Indoluit uicisse parum iam blandior egrum
Non mulcet fortuna animum . carthagine recta
Gloria gestarum sordebat fulgura rerum
Nempe uidebat adhuc profugum longinqua tuentem
Lentaque semineci uibrantem spicula dextra
Turbida quin etiam rumoribus omnia miscens
Fama procul nostro ueniens crescebat ab orbe
Arcibus instantem ausoniis uolitare sub armis
Hanibalem patrieque faces sub menia ferri
FOLIO [3r]
Illustres cecidisse duces ardere nefandis
Ignibus hesperiam . atque undantia cedibus arua
Vrgebat uindicta patris . pietasque mouebat
Vt ceptum sequeretur opus . nam sanguine seuo
Cesorum cineresque sacros umbrasque parentum
Placari atque itala detergi fronte pudorem
Hic amor assiduum pulsabat pectora Clari
Scipiade . in frontem eliciens oculosque iuuenta
Fulgentes calido generosas corde fauillas
Anxia nox operosa dies uix ulla quietis
Hora duci . tanta indomito sub pectore uirtus
Has inter curas ubi sensum amplexibus atris
Nox udam laxabat humum Tytonia quamuis
Vxor adhuc gelidumque senem complexa foueret
Nec dum purpureo nitidas a cardine ualuas
Vellere seu roseas ause reserare fenestras
Excirent dominam famule que secula uoluunt
Fessus et ipse caput posuit tum lumina dulcis
Victa sopor clausit celoque emissa silenti
Vmbra ingens faciesque patris per nubila raptim
Astitit ostendens caro precordia nato
Et latus et multa transfixum cuspide pectus
Diriguit totos inuenis fortissimus artus
Arrecteque horrore come tunc ille pauentem
Corripit et noto permulcens incipit ore
O decus eternum generisque amplissima nostri
Gloria et o patrie tandem spes una labanti
Siste metum : memorique animo mea dicta reconde
Optimus . ecce breuem sed que nisi despicis horam
Multa ferat placitura dedit moderator olimpi
Ille meis uictus precibus stellantia celi
Lumina  per rarum munus - patefecit et ambos
Viuentem penetrare polos permisit ut astra
Me duce et obliquos calles patrieque labores
Atque tuos et adhuc terris ignota sororum
Stamina tum rigido contorum pollice fatum
FOLIO [3v]
Aspicias huc flecte animum uiden illa sub austro
Menia et infami per iura palatia monte
Femineis fundata dolis . Viden ampla furentem
Concilia et tepido stillantem sanguine turbam
Heu nimium nostris urbs insignita ruinis
Heu nuribus trux terra ytalis iterum arma retentas
Fracta semel ? uacuisque iterum struis agmina bustis?
Sic tibrim indomitum segnissime bagrida tempnis ?
Sic modo byrsa ferox Capitolia despicis alta ?
Experiere iterum ? et dominam per uerbera nosces
Is tibi nate labor superest ea gloria iusto
Marte parem factura deis hec uulnera iuro
Sacra michi merito patrie quibus omne refudi
Quod dederat quibus ad superos mauortia uirtus
Fecit iter non ulla meos fodientibus artus
Hostibus atque abeunte anima michi multa dolenti
Occurrisse prius tanti solamina cassus
Quam quod magnanimum post funera nostra uidebam
Vlctorem superesse donum spes ista leuabat
Inde metus alios hinc sensum mortis amare
Talia narrantem percurrit et impia mestis
Vulnera luminibus totumque a uertice corpus
Lustrat adusque pedes . at mens pia prominet extra
Vbertimque fluunt lacrime nec plura parantem
Substinuit . mediisque irrumpens uocibus orsus
Heu heu quid uideo ? quisnam hec michi pectora duro
Confixit mucrone parens ? que dextra uerendam
Gentibus immerito uiolauit sanguine frontem?
Dic genitor nil ante queas committere nostris
Auribus. Hec dicens alto radiantia fletu
Sidera uisus erat sedensque implesse quietas
Infima si liceat summis equare marina
Piscis aqua profugus fluuioque repostus ameno
Non aliter stupeat si iam dulcedine captum
Vis salis insoliti et subitus circumstet amator
Quam sacer ille chorus stupuit namque hactenus ire
FOLIO [4r]
Et dolor et gemitus et mens incerta futuri
Atque metus mortis mundique miserrima nostri
Milia curarum rapide quibus optima uite
Tempora et in tenebris meliores ducimus annos
Illic pura dies quam lux eterna serenat
Quam nec luctus edax nec tristia murmura turbant
Non odia incendunt noua res auremque deorum
Insuetus pulsare fragor pietate recessus
Lucis inaccesse tacitumque impleuerat axem
At pater amplexu cupido precibusque modestis
Occupat et grauibus cohibet suspiria dictis
Parce precor gemitu non hunc tempusque locusque
Exposcunt sed uisa animum si uulnera tangunt
Vsque adeo iuuat et patrios agnoscere casus
Accipe nam paucis perstringam plurima uerbis
Sexta per hesperios penitus uictricia campos
Nostraque signa simul romanaque uiderat estas
Cum michi iam bellique moras curasque peroso
Consilium teste euentu fortuna dedisti
Magnificum infelix . fido cum fratre uiritim
Solicitum partirer onus . geminumque moranti
Incuterem bello calcar . sic alite leua
Distrahimur tandem et scisis legionibus ambo
Insequimur late sparsis regionibus hostem
Nondum plena colis iam stamina nostra sorores
Destituunt fesse . iam mors sua signa relinquit
Illicet imparibus ueriti concurrere fatis
Fraudis opem dubio poscunt in tempore peni
Ars ea certa uiris et nostro cognita damno
Celtarumque animos quibus auxiliaribus arma
Fatris ad id steterant pretio corrumpere adorti
Persuasere fugam . nostrorum exempla per euum
Ante oculos gestanda ducum ne robore freti
Externo proprio non plus in milite fidant
Obicit ille deos ius fas et inania uerba
Raptim abeunt . tacitoque uale uis quanta metallo est
FOLIO [4v]
Dii pudor alma fides uni succumbitis auro
Presidio nudata acies fraterna retrorsum
Auia constituit notosque recurrere montes
Hec uisa est spes una duci premit hostis acerbus
Doctus ad extremum cedenti insistere tergo
Me quoque iam magno distantem punica tractu
Agmina cingebat que clam nouus auxerat hostis
Improbus insultans uisum et michi cedere fato
Nequicquam uetitum caro me iungere fratri
Inferior numero multum tribus undique castris
Vallabar multumque locis urgebar iniquis
Ferrum aderat spes nulla fuge Quod fata sinebant
Tempore in angusto durissima pectora ferro
Pandimus et uafras herebo detrudimus umbras
Ira dolorque dabant animos ars bellica nusquam
Consiliique nichil . Ceu dum uelamina pastor
Fida gerens apibus bellum mouet improbus almis
Nocte sub obscura trepidant mox dulcia meste
Excedunt inopi substrata cubilia cera .
Inde ruunt ceceque fremunt sparsoque uolatu
Importuno instant capiti . stat callidus hostis
Inceptique tenax . postque irrita uulnera uictor
Eruit extirpatque pie cunabula gentis
Sic que sola salus miseris et summa uoluptas
Inuisam iaculis gladioque ultore cohortem
Tundimus et rapidas in uulnere linquimus iras
Illi ex composito stabant ( ceu flantibus austris
Acteus consistit erix . atque astriger athlas
Quid moror ? Incauti armorum sub nube uirumque
Obruimur ? fortuna suum tenet inuida morem
Auersata pios . gelidus michi pectore sanguis
Heserat . agnosco insidias . mortemque propinquam
Nec michi sed patrie metuens pro tempore raptim
Ingredior dictis cuneos firmare labantes
Hac uia preclari miles patet ardua leti
I duce me quem sepe alias maiore profecto es
FOLIO [5r]
Fortuna nusquam fama meliore secutus
Non acies ferri facies non obuia mortis
Terreat exiguo decus ingens sanguine mauors
Obicit et caros illustrat cede nepotes
Nosce genus patriamque libens amplectere sortem
Ignauum fortemque mori . ne tangere damno 
Nature lex una iubet breue tempus utrique
Iam licet et terre pelagique pericula cessent
Vltro aderit suspecta dies . hoc fortibus unum
Contigit ut leti morerentur cetera flendo
Turba perit lacrimasque metu diffundit inertes
Hora breuis longe testis uenit ultima uite
Ergo age si latio quicquam de sanguine restat
Morte palam facito nam dum fortuna sinebat
Vicimus et nostris exibant funera dextris
At modo corporibus cedunt quando omnia retro
Sit satis obstruxisse uia per pectora nostra
Perque truces oculos uultusque in morte tremendos
Transcendant . talem libet his opponere mentem
His claustris nallare aditus . sciat horrida ueros
Barbaries cecidisse uiros . et pallida quamquam
Haud spernenda tamen romana cadauera calcet
Accelera bene nata cohors in limine mors est
Inuidiosa bonis romanas semper ad aras
Cum lacrimis recolenda piis et thure perenni
Talibus accensi coeunt et grandinis instar
Scissa nube ruunt . In tela micantia primus
Et circumfusos feror irrediturus in hostes
Consequitur deuota neci fortissima pubes
Sternimus et morimur ? pacis tot milia contra
Quid reliquum ? Sed fata pii nunc ultima fratris
Expectas . neque enim hesperia felicior ora
Ille quidem extremo fati de turbine frustra
Surgere conatus magne sub mole ruine
Oppressusque itidem nec mors magis ulla decebat
Altera quam fratris fuerat concordia uite
FOLIO [5v]
Mira uel exiguis nunquam interrupta querelis
Vna domus uictusque idem mens una duobus
Et mors una fuit locus idem corpora seruat
Amborum et cineres. huc tempus ferme sub unum
Venimus. hic nobis nulla est iam cura uetusti
Carceris ex alto sparsos contemnimus artus
Odimus et laqueos et uincula nota timemus
Libertatis onus quod nunc sumus illud amamus
Ille autem illacrimans tua me tua care profundo
Corde premit pietas genitor. sed mollis inersque
Vltio uerborum semper fuit  . optima rerum
Dic tamen hoc o sancte parens an uiuere fratrem
Teque putem atque alios quos pridem roma sepultos
Defunctosque uocat ? lente pater ipse loquentem
Risit et o quanta miseri sub nube iacetis
Humanumque genus quanta caligine ueri
Voluitur hec inquit sola est certissima uita
Vestra autem mors est quam uitam dicitis. at tu
Aspice germanum qualis contemptor acerbe
Mortis eat ? Viden domitum sub pectore robur ?
Et uiuum decus et flamantia lumina fronti ?
Quin etiam a tergo generosum aspicis agmen ?
Hos michi defunctos audebit dicere quisquam ?
Et tamen egregios humani sorte tributi
Efflauere animos . ac debita corpora terre
Liquerunt cernis nitido uenientia contra
Per purum radiare diem leta agmina uultu 
Imo ait eximie . nec quicquam dulcius unquam
Hos uidisse oculos memini. sed et omnia nosse
Est animus . tibi ne genitor contraria mens sit
Per superos ipsumque iouem solemque uidentem
Omnia per frigios ( si qua est ea cura ) penates
Per si quid patrie uenit huc dulcedinis oro
Aut ego fallor enim aut quosdam hoc ex agmine noui
Et mores habitusque uirum faciesque gradusque
Insolitum licet ora micent tamen ipse recordor
FOLIO [6r]
Vidi etenim et patria nuper conuiximus urbe
Vera quidem memoras Fraus hunc modo rebus ademit
Punica terrenis periit congressus iniquo
Credulus etate heu nimium marcellus in illa
Ista memor finis lateri latus admouet ultro
Nobiscum ue libens celo spatiatur in amplo
Crispinus longe sequitur quem perfidus uno
Absumpsisse die temptauerat hostis . at illum
Languida dilate tribuerunt uulnera morti
Alter ibi cecidit moriens ubi furta latebant
Inde leuis recte penetrans huc spiritus . illic
Frigida carnifici dimisit membra cruento
En fabium celo maiestas maxima tanti
Nominis ac rerum iubet habitare sereno
Cerne ducem quantum licet hic cunctator ab omni
Dictus erat populo tamen ingens gloria tardis
Debita consiliis uiget hunc non flama non ensis
Erripuit latio . sed dum magis arma premebant
Punica tranquillum tulit huc annosa senectus
Sed magis ardentemque animis pugnasque frementem
Cer per insidias indigno funere graccum
Corpore se clusum ualido et pollentibus armis
Preterea emilio nimium fors inuida paulo
Aspice magnanimum terebrant quot uulnera pectus ?
Cannensi romana die defleta supremum
Fata putans renuit cladi superesse sed ultro
Oblatum contempsit equum multumque rogantem
Reppulit et nimium respondit uiximus at tu
Matte animi uirtute puer discede tuumque
Victurum abde caput teque ad meliora reserua
Dic patribus muniant urbem dic menia fument
Condistant extrema pati namque improba seuas
Ingeminat fortuna minas hostisque cruentus
Victor adest mea uerba fabio nouissima perfer
Dic me iussorum memorem uixisse suorum
Dic memorem te teste mori sed fata feroxque
FOLIO [6v]
Collega ingenti turbarunt cuncta tumultu
Nuda loco caruit uirtus tulit impetus illam
Effuge dum morior ne forsan plura loquendo
Sim tibi causa necis ( dicentem talia ferro
Circumstant uolat ille leuis timor alleuat artus
Et plumas adiungit equo et calcaria plantis
Anxia ceu uolucris ubi nidum callidus anguis
Obsidet hinc uise sese subducere morti
Optat et hinc dubitat sua dulcia uiscera linquens
Infelix pietas tandem formidine uicta
Cedit . et incussis serum sibi consulit alis ?
Vicinaque tremens respectat ab arbore fatum
Natorum rabiemque fere et plangoribus omne
Implet anhela nemus strepituque accurrit amico
Sic ibat iuuenis memorandus sepe retrorsum
Lumina mesta ferens uidet ingens surgere campis
Naufragium uidet immitem post publica penum
Funera sacra ducis fodientem pectora duris
Ictibus et celum gemitu pulsabat inani.
Quid moror ? Innumeram licet inter noscere turbam
Cesorum hoc bello iuuenum patrieque cadentum
Scilicet immenso studio dum ledere querit
Ciuibus atque inopem spoliat dum fortibus urbem
Compleuit celum nostris ferus hanibal umbris
Talia dum genitor memorat suspiria natus
Alta trahens . licuit fateor cognoscere quicquid
Optabam magis ! et uultus spectare meorum
Cetera ni prohibes . nichil est sermone secundi
Patris amabilius : Quin tu modo cominus inquit
Alloquere atque aures quam primum inuade paratas
His dictis tulit ante gradum frontemque modestam
Demisit patruumque tenens sic incipit ore
O uenerande michi uero nunquamque parente
Care minus si uestra deus dedit ora uidere
Mortales oculos alti si lumina mundi
Indignoque michi clarum reserauit olimpum
FOLIO [7r]
Da precor exiguam nostris affatibus horam
Nam breue tempus adest . moneorque in castra reuerti
Occeani subnixa uadis ubi maxima calpes
Impendet pelago celumque cacumine pulsat
Illic me romana manet modo signa ducemque
Expectant . rapidum hoc tandem stat limite bellum
Suscipit amplexu iuuenem placidissimus heros
Atque ita si iussu superum mortalia celo
Membra uehis nec enim tam magni muneris auctor
Alter erit summum hoc equidem tibi contigit uni
Eximiumque decus quam de te concipiam spem
Dictu dificile est. Cui tanta numina uiuo
Concessere uiam nam ni diuinus inesset
Spiritus haud quamquam hoc homini fortuna dedisset
Que faciles despensat opes archana uidere
Celica uenturos longe pronoscere casus
Et fatum prescire suum . spectare beatas
Has animas subterque pedes radiantia solis
Lumina et aduersos tam uastis tractibus axes
Hec nunquam fortuna dabit . quia cuncta potenti
Sunt seruata deo ! qui si te lumine tanto
Illustrat quonam te alii dignentur honore?
Non ergo immerito fractos passimque iacentes
Hesperie campis totiens despeximus hostes
Vidimus et nostre uindictam mortis ab illa
Egregie pietatis habes per specula famam
Quidlibet hic audet mecum : nam protinus aurem
Inuenis atque animum uacuum ! quin ocius ergo
Ingredimur : fandoque breuem consumimus horam
Dic ait is si uita manet post busta . quod almus
Testatur genitor . sique hec est uera perennis
Nostra autem morti similis quid demoror ultra
In terris ? quin huc potius quacumque licebit
Euolat assurgens animus tellure relicta ?
Non bene sentis ait deus hoc naturaque sanxit
Legibus eternis hominem statione manere
FOLIO [7v]
Corporis edicto donec reuocetur aperto
Non igitur properare decet : sed ferre modeste
Quantulacumque breuis . superant incommoda uite
Ne uisum spreuisse dei uideare quod ista
Sunt geniti sub lege homines ut regna tenerent
Infima namque illic custodia credita terre
Et rerum quas terra uehit pelagusque profundum
Ergo tibi cunctisque bonis seruandus in ista est
Carne animus propriamque uetandus linquere sedem
Nobilibus curis studioque et amore uidendi
Promineat ni forte foras corpusque relinquit
Ac longe fugiat sensus seque ingerat astris
Hic decet egregios animos . hic exitus est quem
Diuini fecere uiri meliora sequentes
Sed dum membra uigent breuis est mora suscipe nostri
Consilii quid summa uelit tu sacra fidemque
Iustitiamque cole pietas sit pectoris hospes
Sancta tui morumque comes que debita uirtus
Magna patri patrie maior sed maxima summo
Ac perfecta deo . quibus exornata profecto
Vita uia in celum est que nos huc tramite recto
Tunc reuehat cum summa dies exemerit istud
Carnis onus pureque animam transmiserit aure
Hoc etiam monuisse uelim nil gratius illi
Qui celum terrasque regit dominoque patrique
Actibus ex uestris quam iustis legibus urbes
Conciliumque hominum sociatum nexibus equis
Quisquis enim ingenio patriam seu uiribus alte
Sustulerit sumptisque oppressam iuuerit armis
Hic certum sine fine locum in regione serena
Expectet uereque petat sibi premia uite
Iustitia statuente dei . que nec quid in ultum
Nec premio caruisse sinit ( sic fatus amoris
Admouitque facies auido stimulosque nepoti
Ecce autem interea uenientum turba nec ulli
Nota fuit facies . habitus tamen omnibus unus
FOLIO [8r]
Sidereoque leuis fulgebat lumine amictus
Augusta pauci procul omnes fronte preibant
Iam senioque graues et maiestate uerendi
Hec acies regum est quos tempora prima tulerunt
Vrbis ait nostre . frons arguit inclita reges
Romulus ecce prior famosi nominis auctor
Publicus ille parens . cernis dulcissime quantus
Ardor inest animo . talem uentura petebant
Regna uirum . Venit incessu moderatior alter
Religione noua populum qui temperet acrem
Hic uirtute prius patrie curibusque sabinis
Insignis nostramque ideo trasuectus in arcem est
Aspice solicitum monitu ceu coniugis almas
Instituat leges . et euntem diuidat annum
Extulit hunc natura senem . primoque sub euo
Hanc habuit frontem sic tempora cana genasque
Tertius ille sequens qua tu nunc uteris omnem
Militie expressit regum fortissimus artem
Fulmineus uisu uictus quoque fulmine solo
Quartus arat muros et tibridis hostia fundat
Presagus quas totus opes huc conuehat orbis
Addita primeuo connectens menia ponte
Frons quinti michi nota parum sed suspicor illum
Quem nobis regem longe dedit alta corintus
Ille est haud dubie ! uideo tunicasque togasque
Et fasces trabeasque graues sellasque curules
Atque leues faleras et cuncta insignia nostri
Imperii currusque et equos pompasque triumphi
Illum autem numero quem cernis in ordine sextum
Seruilis solio regem transmisit origo
Et nomen seruile manet . sed regia mens est
Dedecus hic generis uirtute piauit et actis
Condidit hic censum prior ut se noscere posset
Roma potens . altumque nichil sibi nota timeret
Finierat . tunc ille iterat si lecta recordor
Romuleo cinxisse comas diademate septem
FOLIO [8v]
Audieram totidem cognomina certa tenebam
Alter ubi est igitur Fili carissime dixit
Huc et luxus et dura superbia nunquam
Ascendunt illum sua pessima crimina auerno
Merserunt atrox animus nomenque superbum
Hunc exposcis enim qui sceptra nouissima rexit
Rex ferus et feritate bonus . nam tristia passe
Hic libertatis primum urbi incessit amorem
Quin animas letas melioraque regna tenentes
Cerne catheruatim uere uirtutis amicas
Tres simul ante alacres alternaque brachia nexi
Ibant . hos leto celebrabant agmina plausu
Vmbrarum . atque omni deuotum ex ordiene uulgus
Substitit admirans que tanta est gratia dixit
Ista trium ? Quis tantus amor conectit euntes ?
Hos idemque parens eademque ait extulit aluus
Hinc amor hisque ipsis libertas credita quondam
Hinc fauor : heu iugulos et uulnera cruda duorum
Aspice : utrique ridens nitet ut generosa cicatrix
Pectore in aduerso : populorum pugna potentum
Tergeminis mandata uiris ut sanguine pauco
Scilicet innumere cessarent funera gentis
Libertas nunc nostra tremens similisque cadenti
Vnius ad fatum dubio sub marte pependit
Vnius est asserta manu germanus uterque
Occiderat populosque nimis fortuna fauere
Ceperat albano ! nisi tertius ille superstes
Integer et fratrum mortes et publica fata
Restituisset agens uictritia corpora campo
Donec se iunctos spaciis ! largoque cruore
Defectos pelagisque graues et cursibus haustos
Impiger alterno iugulasset uulnere fratres
Id recolens nunc exultat gaudentque uicissim
Germani ad superos nec inulto funere missi
At quibus imperium uirtus ea contulit ultro
Circumstant memores ? Sed quid per singula uersor
FOLIO [9r]
Milia nonne uides spatosum implentia celum ?
Publicolam ante alios tanto cognomine dignum
Preclarum pietate ducem patrieque parentem
Lumina uisendi cupidus flectebat et ingens
Agmen erat iuxta . stabilem qua uergit ad arthon
Lacteus innumeris redimitus circulus astris
Obstupuit . queritque uiros et nomina et actus
Care nepos si cuncta uelim memoranda referre
Altera nox optanda tibi est ait aspice ut omnis
Stella cadit pelago . celumque reflectitur et iam
Candidus aurore meditantis surgere uultus
Vibrat et eoa iam somnum diluit unda
Tum pater admonuit fugientia sidera nutu
Ostendens uetuitque moras . hoc nosse satis est
Romanas has esse animas quibus una tuende
Cura fuit patrie . proprio pars magna cruore
Diffuso has petiit sedes meritoque caduce
Pretulit eternam per acerba pericula uictam
